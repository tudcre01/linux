/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (C) 2021 Arm Ltd.
 */

#define COPY_FUNC_NAME __arch_copy_from_user_with_captags
#define COPY_CAPTAGS
#include "copy_from_user.S"
